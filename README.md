# wad-scorecard
A score-keeping spreadsheet for doom and beyond. 
---

This is a project that aims to create a simple to use scorecard for tracking doom and doomlike
usermade content (wads) in one simple, easy to use way. Currently this is a spreadsheet with
multiple subsheets.

### It has the following features.
* An overall stat tracking sheet at the front for an easy way seeing various stats including checklist completion, skill completion and wad and map counting. (Done automatically)
* A master list for tracking all the wads you have, icluding selectable status, selectable ratings, and notes. (Filled in Manually)
* A map list for tracking the individual maps that comprise the wads and megawads, including various completion criteria and a collumn for tracking your demos. (Manually Filled)
* A checklist for Doomworld's cacowards.
* A checklist for the Doomer Boards Projects.
* A checklist for stand-out community projects.
* A checklist for other lists.
* A sheet template for tracking various mods various other things you may wish to track.

### It is currently available in two versions in two formats.
* The scorcard is written and developed as an odt document, but also includes an export for the proprietary Microsoft format via LO Calc's export function.
* The "Fresh" version is almost completely blank for those who wish to start clean. Icludes a handfull of example entries to get you started.
* The "Preseasoned" version which comes the master list prefilled with every entry from the checklists and the map list comes partialy prefilled with the maps.(Work in progress)

A note on using the version from the repo. If you want the absolute latest working copy, you need to
open the scorecard ods file in an archive manager and place the Pictures folder into it. I've separated
the inserted art from the actual spreadsheet to keep file sizes down with git, and to make tracking
some changes easier.
This project is a work in progress. If you spot any mistakes or ommissions, feel free to let me know..

---
Special thanks to Doomworld and their users from which the cacoward art is taken,
and to the doomwiki for the wealth of information.
---
You can find this on Doomworld here:https://www.doomworld.com/forum/topic/133645
